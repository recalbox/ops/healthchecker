#!/bin/bash
set -eu 

# Check recalbox websites (forum, gitbook, www)
echo "Checking https://www.recalbox.com"
curl -fs https://www.recalbox.com/fr/ | grep "<title data-react-helmet=\"true\">Recalbox Home | recalbox.com</title>" > /dev/null || ( echo "https://www.recalbox.com is down" && exit 1)

echo "Checking https://forum.recalbox.com"
curl -fs https://forum.recalbox.com | grep "<title>Home | Recalbox Forum</title>" > /dev/null || ( echo "https://forum.recalbox.com is down" && exit 1 )

echo "Checking https://wiki.recalbox.com/en/home"
curl -fs https://wiki.recalbox.com/en/home > /dev/null || ( echo "https://wiki.recalbox.com/en/home is down" && exit 1 )
