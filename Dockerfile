FROM ubuntu:20.04

ENV RECALBOX_CURRENT_VERSION ""
ENV RECALBOX_DISCORD_URL ""
ENV RECALBOX_SLACK_URL ""

RUN apt-get update && apt-get -y install dnsutils curl jq
ADD . /app
WORKDIR /app

CMD bash ./run-all-scripts.sh
