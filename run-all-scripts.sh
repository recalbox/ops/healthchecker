#!/bin/bash
set -u
set -o pipefail


for script in check*.sh;do 
  bash < "${script}" 2>&1 | tee ${script}.log
  if [ $? != 0 ];then
    curl -s -f -X POST --data-urlencode \
      "payload={\"channel\": \"#site-monitoring\", \"username\": \"healthchecker\",  \"icon_emoji\": \":heartbeat:\", \"text\": $(tail ${script}.log | jq -Rs)}}" \
      "${RECALBOX_SLACK_URL}"
    curl -s -f -X POST -H "Content-Type: application/json" -d \
      "{\"username\": \"healthchecker\", \"content\": $(tail ${script}.log | jq -Rs)}" \
      "${RECALBOX_DISCORD_URL}"
  fi
done