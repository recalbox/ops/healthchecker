#!/bin/bash
set -eu 

# Check release images and recalbox.version on every recalbox upgrade server
# RECALBOX_CURRENT_VERSION must be in the environment variables
function checkForDomain(){
  echo 
  domain="$1"
  checkversion="$2"
  version="$3"
  echo "Checking domain : ${domain}"
  for ip in $(nslookup ${domain} 1.1.1.1 | grep Address | tail -n+2 | awk '{print $2}');do 
    for arch in "${@:4}"; do 
      # Version
      echo "Testing https://${domain}/latest/$arch/recalbox.version on $ip" 
      set +e
      theversion=$(curl -s -f "https://${domain}/latest/$arch/recalbox.version?source=testing" --resolve ${domain}:443:$ip)
      set -e
      if [[ $? != 0 ]]; then
        echo "https://${domain}/latest/$arch/recalbox.version download failed on $ip" && exit 1
      fi
      if [[ "${checkversion}" == "1" && "${theversion}" != "${version}" ]]; then
        echo "https://${domain}/latest/$arch/recalbox.version does not correspond to ${version} on $ip for $arch" && exit 1
      fi

      # Image with source in path (test redirect of proxies)
      echo "Testing https://${domain}/latest/testing/$arch/recalbox-${arch}.img.xz on $ip" 
      curl -s -f -I https://${domain}/latest/testing/$arch/recalbox-${arch}.img.xz --resolve ${domain}:443:$ip > /dev/null \
      || ( echo "recalbox.img.xz check failed on $ip" && exit 1 )

      done 
  done
}

checkForDomain upgrade.recalbox.com 1 "8.0.1-Electron" rpi2
checkForDomain upgrade.recalbox.com 1 "7.2.2-Reloaded" x86
checkForDomain upgrade.recalbox.com 1 "${RECALBOX_CURRENT_VERSION}" rpi1 rpi3 rpi4_64 rpizero2legacy odroidxu4 odroidgo2 x86_64
#checkForDomain beta.recalbox.com 0 rpizero2 rpizero2legacy
